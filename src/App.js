import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import routes from './routes'
import './App.css'

// wrap <Route> and use this everywhere instead, then when
// sub routes are added to any route it'll work
const RouteWithSubRoutes = route => {
  return (
    <Route
      path={route.path}
      render={props => {
        return (
      // pass the sub-routes down to keep nesting
          <route.component {...props} {...route} />
    )
      }}
    />
  )
}


const App = (props) => {
  return (
    <Router>
      <div>
        {routes.map(route => {
          return (
            <RouteWithSubRoutes key={route.path} {...route} {...props} />
          )
        })}
      </div>
    </Router>
  )
}

export default App
