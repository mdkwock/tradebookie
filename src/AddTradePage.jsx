import React from 'react'
import { observer } from 'mobx-react'
import Page from './Page'
import AddTradeForm from './components/AddTradeForm/AddTradeForm'
import './App.css'

const propTypes = {
  UIState: React.PropTypes.shape({
    setFormState: React.PropTypes.func,
    changeFormState: React.PropTypes.func,
    getForm: React.PropTypes.func,
  }).isRequired,
  TradeStore: React.PropTypes.shape({
    createTrade: React.PropTypes.func
  }).isRequired
}

const AddTradePage = observer(({ UIState, TradeStore }) => {
  const addTradeFormId = 'add-trade-form-id'
  const formData = UIState.getForm(addTradeFormId)
  return (
    <Page title="Add a Trade">
      <AddTradeForm
        setFormState={UIState.setFormState}
        changeFormState={UIState.changeFormState}
        formState={formData}
        id={addTradeFormId}
        onSubmit={TradeStore.createTrade}
      />
    </Page>
  )
})

AddTradePage.propTypes = propTypes

export default AddTradePage
