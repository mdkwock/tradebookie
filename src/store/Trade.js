import { action, computed, observable } from 'mobx'
import uuid from 'uuid'

/**
 * A trade has
 * a trade timestamp
 * a stock symbol
 * a price
 * a quantity
 * buy or sell
 * tags
 */

export default class Trade {

  /**
   * unique id of this trade, immutable.
   */
  id = null;
  store = null;

  @observable timestamp = new Date()
  @observable stockSymbol = ''
  @observable price = 0
  @observable quantity = 0
  @observable type = ''
  @observable tags = []

  constructor(store, id = uuid.v4()) {
    this.store = store
    this.id = id
  }

  @computed get asJson() {
    return {
      id: this.id,
      timestamp: this.timestamp,
      stockSymbol: this.stockSymbol,
      price: this.price,
      quantity: this.quantity,
      type: this.type,
      tags: this.tags.peek(),
    }
  }

  @action updateTrade({
    timestamp,
    stockSymbol,
    price,
    quantity,
    type,
    tags,
  }) {
    this.timestamp = timestamp || this.timestamp
    this.stockSymbol = stockSymbol || this.stockSymbol
    this.price = price || this.price
    this.quantity = quantity || this.quantity
    this.type = type || this.type
    this.tags = tags || this.tags
  }
}
