import { action, computed, observable } from 'mobx'

/**
 * A stock has
 * a stock symbol
 * a name
 * (not sure if we need a different id yet)
 */

export default class Stock {

  store = null;

  symbol = ''
  @observable name = ''

  constructor(store, symbol) {
    this.store = store
    this.symbol = symbol
  }

  @computed get asJson() {
    return {
      symbol: this.symbol,
      name: this.name,
    }
  }

  @action updateStock({ name }) {
    this.name = name
  }
}
