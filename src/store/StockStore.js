import { observable } from 'mobx'
import Stock from './Stock'

export default class StockStore {
  transportLayer;
  @observable stocks = [];
  @observable isLoading = true;

  constructor(transportLayer = null) {
    this.transportLayer = transportLayer // Thing that can make server requests for us
    this.updateStockFromServer = this.updateStockFromServer.bind(this)
    this.getStock = this.getStock.bind(this)
    this.createStock = this.createStock.bind(this)
    this.removeStock = this.removeStock.bind(this)
  }

  /**
   * Fetches all stock's from the server
   */
  loadstocks() {
  }

  /**
   * Update a stock with information from the server. Guarantees a stock
   * only exists once. Might either construct a new stock, update an existing one,
   * or remove an stock if it has been deleted on the server.
   */
  updateStockFromServer(json) {
    let stock = this.stocks.find((s) => { return s.symbol === json.symbol })
    if (!stock) {
      stock = new Stock(this, json.symbol)
      this.stocks.push(stock)
    }
    if (json.isDeleted) {
      this.removestock(stock)
    } else {
      stock.updateStock(json)
    }
  }

  getStock(stockSymbol) {
    const stock = this.stocks.find((s) => { return s.symbol === stockSymbol })
    return stock
  }

  /**
   * Creates a fresh stock on the client and server
   */
  createStock(stockSymbol) {
    let stock = this.stocks.find((s) => { return s.symbol === stockSymbol })
    if (stock) {
      return stock
    }
    stock = new Stock(this, stockSymbol)
    this.stocks.push(stock)
    return stock
  }

  /**
   * A stock was somehow deleted, clean it from the client memory
   */
  removeStock(stockSymbol) {
    const index = this.stocks.findIndex((s) => { return s.symbol === stockSymbol })
    if (index !== -1) {
      this.stocks.splice(index, 1)
    }
  }
}
