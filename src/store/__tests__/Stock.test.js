import Stock from '../Stock'

describe('Stock Object Tests', () => {
  const stockJson = {
    symbol: 'test',
    name: 'test company name',
  }
  it('Stock Object gets created', () => {
    const stock = new Stock(null, stockJson.symbol)
    expect(typeof stock).toBe('object')
    expect(stock.symbol).toBe(stockJson.symbol)
  })
  it('updateStock works', () => {
    const stock = new Stock(null, stockJson.symbol)
    stock.updateStock(stockJson)
    expect(stock.symbol).toBe(stockJson.symbol)
    expect(stock.name).toBe(stockJson.name)
  })
  it('asJson works', () => {
    const stock = new Stock(null, stockJson.symbol)
    stock.updateStock(stockJson)
    expect(stock.asJson).toEqual(stockJson)
  })
})
