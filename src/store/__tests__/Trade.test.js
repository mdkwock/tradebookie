import Trade from '../Trade'

describe('Trade Object Tests', () => {
  const tradeJson = {
    id: 'test-id',
    timestamp: '2013-11-05 01:01:01',
    stockSymbol: 'test',
    price: 4,
    quantity: 4,
    type: 'buy',
    tags: ['tag1'],
  }
  it('Trade Object gets created', () => {
    const trade = new Trade(null, tradeJson.id)
    expect(typeof trade).toBe('object')
    expect(trade.id).toBe(tradeJson.id)
  })
  it("Trade object can create it's own id", () => {
    const trade = new Trade(null)
    expect(typeof trade).toBe('object')
    expect(trade.id).toBeDefined()
  })
  it('updateTrade works', () => {
    const trade = new Trade(null, tradeJson.id)
    trade.updateTrade(tradeJson)
    expect(trade.timestamp).toBe(tradeJson.timestamp)
    expect(trade.stockSymbol).toBe(tradeJson.stockSymbol)
    expect(trade.price).toBe(tradeJson.price)
    expect(trade.quantity).toBe(tradeJson.quantity)
    expect(trade.type).toBe(tradeJson.type)
    expect(trade.tags.peek()).toEqual(tradeJson.tags)
  })
  it('asJson works', () => {
    const trade = new Trade(null, tradeJson.id)
    trade.updateTrade(tradeJson)
    expect(trade.asJson).toEqual(tradeJson)
  })
})
