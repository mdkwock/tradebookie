import TradeStore from '../TradeStore'
import Trade from '../Trade'

describe('TradeStore Tests', () => {
  const tradeStore = new TradeStore()
  const tradeJson = {
    id: 'id',
    timestamp: 'time',
    stockSymbol: 'stockSymbol',
    price: 4,
    quantity: 3,
    type: 'buy',
    tags: [],
  }
  const trade2Json = {
    id: 'id2',
    timestamp: 'time',
    stockSymbol: 'stockSymbol',
    price: 4,
    quantity: 3,
    type: 'buy',
    tags: [],
  }
  it('trade store has trades', () => {
    expect(tradeStore.trades).toHaveLength(0)
  })
  describe('updateTradeFromServer tests', () => {
    it('if there are no trades, it will add a trade', () => {
      tradeStore.trades.clear()
      expect(tradeStore.trades).toHaveLength(0)

      tradeStore.updateTradeFromServer(tradeJson)
      expect(tradeStore.trades).toHaveLength(1)
      expect(tradeStore.trades[0]).toBeInstanceOf(Trade)
      expect(tradeStore.trades[0].asJson).toEqual(tradeJson)
    })
    it('if there are no trade with the id, it will add a trade', () => {
      tradeStore.trades.clear()
      expect(tradeStore.trades).toHaveLength(0)

      tradeStore.updateTradeFromServer(tradeJson)
      expect(tradeStore.trades[0].asJson).toEqual(tradeJson)
      expect(tradeStore.trades).toHaveLength(1)

      tradeStore.updateTradeFromServer(trade2Json)
      expect(tradeStore.trades[1].asJson).toEqual(trade2Json)
      expect(tradeStore.trades).toHaveLength(2)
    })
    it('if there is a trade with the same id, it will update', () => {
      tradeStore.trades.clear()
      expect(tradeStore.trades).toHaveLength(0)

      tradeStore.updateTradeFromServer(tradeJson)
      expect(tradeStore.trades[0].asJson).toEqual(tradeJson)
      expect(tradeStore.trades).toHaveLength(1)

      const tradeTempJson = tradeJson
      tradeTempJson.stockSymbol = 'stocks'
      tradeStore.updateTradeFromServer(tradeTempJson)
      expect(tradeStore.trades[0].asJson).toEqual(tradeTempJson)
      expect(tradeStore.trades).toHaveLength(1)
    })
  })
  describe('getTrade tests', () => {
    it('if there are no trades, it will return undefined', () => {
      tradeStore.trades.clear()
      expect(tradeStore.trades).toHaveLength(0)
      const trade = tradeStore.getTrade(tradeJson.id)
      expect(trade).toBeUndefined()
    })
    it('if there are no trade with the id, it will return undefined', () => {
      tradeStore.trades.clear()
      expect(tradeStore.trades).toHaveLength(0)
      tradeStore.updateTradeFromServer(tradeJson)
      tradeStore.updateTradeFromServer(trade2Json)
      const trade = tradeStore.getTrade('id3')

      expect(trade).toBeUndefined()
    })
    it('if there is a trade with the same id, it will return the trade', () => {
      tradeStore.trades.clear()
      expect(tradeStore.trades).toHaveLength(0)
      tradeStore.updateTradeFromServer(tradeJson)
      tradeStore.updateTradeFromServer(trade2Json)
      const trade = tradeStore.getTrade(trade2Json.id)

      expect(trade).not.toBeUndefined()
      expect(trade.asJson).toEqual(trade2Json)
    })
  })
  describe('createTrade tests', () => {
    it('if there are no trades, it will add a trade', () => {
      tradeStore.trades.clear()
      expect(tradeStore.trades).toHaveLength(0)
      const trade = tradeStore.createTrade()

      expect(tradeStore.trades).toHaveLength(1)
      expect(tradeStore.trades[0]).toBeInstanceOf(Trade)
      expect(trade).toBeInstanceOf(Trade)
    })
    it('if there are trades, it will add a trade to the list', () => {
      tradeStore.trades.clear()
      expect(tradeStore.trades).toHaveLength(0)

      const trade = tradeStore.createTrade('tradeSymbol')
      expect(tradeStore.trades).toHaveLength(1)
      expect(tradeStore.trades[0]).toBeInstanceOf(Trade)
      expect(trade).toBeInstanceOf(Trade)

      const trade2 = tradeStore.createTrade('tradeSymbol2')
      expect(tradeStore.trades).toHaveLength(2)
      expect(tradeStore.trades[0]).toBeInstanceOf(Trade)
      expect(trade).toBeInstanceOf(Trade)
      expect(trade.id).not.toBe(trade2.id)
    })
  })
  describe('removeTrades tests', () => {
    it('if there are no trades, it will do nothing', () => {
      tradeStore.trades.clear()
      expect(tradeStore.trades).toHaveLength(0)

      tradeStore.removeTrade('id2')
      expect(tradeStore.trades).toHaveLength(0)
    })
    it('if there are no trade with the id, it will do nothing', () => {
      tradeStore.trades.clear()
      expect(tradeStore.trades).toHaveLength(0)

      tradeStore.updateTradeFromServer(tradeJson)
      expect(tradeStore.trades).toHaveLength(1)
      tradeStore.removeTrade('id2')
      expect(tradeStore.trades).toHaveLength(1)
    })
    it('if there is a trade with the same id, it will remove that trade', () => {
      tradeStore.trades.clear()
      expect(tradeStore.trades).toHaveLength(0)

      tradeStore.updateTradeFromServer(tradeJson)
      tradeStore.updateTradeFromServer(trade2Json)
      expect(tradeStore.trades).toHaveLength(2)
      tradeStore.removeTrade(trade2Json.id)
      expect(tradeStore.trades).toHaveLength(1)
    })
  })
})
