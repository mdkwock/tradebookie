import StockStore from '../StockStore'
import Stock from '../Stock'

describe('StockStore Tests', () => {
  const stockStore = new StockStore()
  it('stock store has stocks', () => {
    expect(stockStore.stocks).toHaveLength(0)
  })
  describe('updateStockFromServer tests', () => {
    it('if there are no stocks, it will add a stock', () => {
      stockStore.stocks.clear()
      expect(stockStore.stocks).toHaveLength(0)

      stockStore.updateStockFromServer({ symbol: 'stockSymbol', name: 'test' })
      expect(stockStore.stocks).toHaveLength(1)
      expect(stockStore.stocks[0]).toBeInstanceOf(Stock)
      expect(stockStore.stocks[0].asJson).toEqual({ symbol: 'stockSymbol', name: 'test' })
    })
    it('if there are no stock with the id, it will add a stock', () => {
      stockStore.stocks.clear()
      expect(stockStore.stocks).toHaveLength(0)

      stockStore.updateStockFromServer({ symbol: 'stockSymbol', name: 'test' })
      expect(stockStore.stocks[0].asJson).toEqual({ symbol: 'stockSymbol', name: 'test' })
      expect(stockStore.stocks).toHaveLength(1)

      stockStore.updateStockFromServer({ symbol: 'stockSymbol2', name: 'test' })
      expect(stockStore.stocks[1].asJson).toEqual({ symbol: 'stockSymbol2', name: 'test' })
      expect(stockStore.stocks).toHaveLength(2)
    })
    it('if there is a stock with the same id, it will update', () => {
      stockStore.stocks.clear()
      expect(stockStore.stocks).toHaveLength(0)

      stockStore.updateStockFromServer({ symbol: 'stockSymbol', name: 'test' })
      expect(stockStore.stocks[0].asJson).toEqual({ symbol: 'stockSymbol', name: 'test' })
      expect(stockStore.stocks).toHaveLength(1)

      stockStore.updateStockFromServer({ symbol: 'stockSymbol', name: 'test2' })
      expect(stockStore.stocks[0].asJson).toEqual({ symbol: 'stockSymbol', name: 'test2' })
      expect(stockStore.stocks).toHaveLength(1)
    })
  })
  describe('getStock tests', () => {
    it('if there are no stocks, it will return undefined', () => {
      stockStore.stocks.clear()
      expect(stockStore.stocks).toHaveLength(0)
      const stock = stockStore.getStock('stockSymbol')
      expect(stock).toBeUndefined()
    })
    it('if there are no stock with the id, it will return undefined', () => {
      stockStore.stocks.clear()
      expect(stockStore.stocks).toHaveLength(0)
      stockStore.updateStockFromServer({ symbol: 'stockSymbol', name: 'test' })
      stockStore.updateStockFromServer({ symbol: 'stockSymbol2', name: 'test2' })
      const stock = stockStore.getStock('stockSymbol3')

      expect(stock).toBeUndefined()
    })
    it('if there is a stock with the same id, it will return the stock', () => {
      stockStore.stocks.clear()
      expect(stockStore.stocks).toHaveLength(0)
      stockStore.updateStockFromServer({ symbol: 'stockSymbol', name: 'test' })
      stockStore.updateStockFromServer({ symbol: 'stockSymbol2', name: 'test2' })
      const stock = stockStore.getStock('stockSymbol2')

      expect(stock.asJson).toEqual({ symbol: 'stockSymbol2', name: 'test2' })
    })
  })
  describe('createStock tests', () => {
    it('if there are no stocks, it will add a stock', () => {
      stockStore.stocks.clear()
      expect(stockStore.stocks).toHaveLength(0)
      const stock = stockStore.createStock('stockSymbol')

      expect(stockStore.stocks).toHaveLength(1)
      expect(stockStore.stocks[0].asJson).toEqual({ symbol: 'stockSymbol', name: '' })
      expect(stock.asJson).toEqual({ symbol: 'stockSymbol', name: '' })
    })
    it('if there are no stock with the id, it will add a stock', () => {
      stockStore.stocks.clear()
      expect(stockStore.stocks).toHaveLength(0)

      let stock = stockStore.createStock('stockSymbol')
      expect(stockStore.stocks).toHaveLength(1)
      expect(stockStore.stocks[0].asJson).toEqual({ symbol: 'stockSymbol', name: '' })
      expect(stock.asJson).toEqual({ symbol: 'stockSymbol', name: '' })

      stock = stockStore.createStock('stockSymbol2')
      expect(stockStore.stocks).toHaveLength(2)
      expect(stockStore.stocks[1].asJson).toEqual({ symbol: 'stockSymbol2', name: '' })
      expect(stock.asJson).toEqual({ symbol: 'stockSymbol2', name: '' })
    })
    it('if there is a stock with the same id, it will add a stock', () => {
      stockStore.stocks.clear()
      expect(stockStore.stocks).toHaveLength(0)
      const stock = stockStore.createStock('stockSymbol')
      expect(stockStore.stocks).toHaveLength(1)
      expect(stockStore.stocks[0].asJson).toEqual({ symbol: 'stockSymbol', name: '' })
      expect(stock.asJson).toEqual({ symbol: 'stockSymbol', name: '' })

      const stock2 = stockStore.createStock('stockSymbol')
      expect(stockStore.stocks).toHaveLength(1)
      expect(stockStore.stocks[0].asJson).toEqual({ symbol: 'stockSymbol', name: '' })
      expect(stock2.asJson).toEqual({ symbol: 'stockSymbol', name: '' })
      expect(stock).toBe(stock2)
    })
  })
  describe('removeStocks tests', () => {
    it('if there are no stocks, it will do nothing', () => {
      stockStore.stocks.clear()
      expect(stockStore.stocks).toHaveLength(0)

      stockStore.removeStock('stockSymbol')
      expect(stockStore.stocks).toHaveLength(0)
    })
    it('if there are no stock with the id, it will do nothing', () => {
      stockStore.stocks.clear()
      expect(stockStore.stocks).toHaveLength(0)

      stockStore.createStock('stockSymbol')
      expect(stockStore.stocks).toHaveLength(1)
      stockStore.removeStock('stockSymbol2')
      expect(stockStore.stocks).toHaveLength(1)
    })
    it('if there is a stock with the same id, it will remove that stock', () => {
      stockStore.stocks.clear()
      expect(stockStore.stocks).toHaveLength(0)

      stockStore.createStock('stockSymbol')
      stockStore.createStock('stockSymbol2')
      expect(stockStore.stocks).toHaveLength(2)
      stockStore.removeStock('stockSymbol2')
      expect(stockStore.stocks).toHaveLength(1)
    })
  })
})
