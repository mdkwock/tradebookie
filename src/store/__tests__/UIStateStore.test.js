import UIStateStore from '../UIStateStore'

describe('UI State Tests', () => {
  it('UI state is a singleton', () => {
    expect(typeof UIStateStore).toBe('object')
  })
  describe('Forms State tests', () => {
    it('UI state has forms', () => {
      expect(UIStateStore.formsState).toHaveLength(0)
    })
    describe('getForm tests', () => {
      it('if there are no forms, it returns undefined', () => {
        UIStateStore.formsState.clear()
        expect(UIStateStore.formsState).toHaveLength(0)
        const form = UIStateStore.getForm('test-id')
        expect(form).toBeUndefined()
      })
      it('if there are no forms with the id, it returns undefined', () => {
        UIStateStore.formsState.clear()
        expect(UIStateStore.formsState).toHaveLength(0)
        UIStateStore.setFormState('test-id', { test: 'test' })
        const form = UIStateStore.getForm('test-id2')
        expect(form).toBeUndefined()
      })
      it('if there is a form with the id, it returns the form', () => {
        UIStateStore.formsState.clear()
        expect(UIStateStore.formsState).toHaveLength(0)
        UIStateStore.setFormState('test-id', { test: 'test' })
        UIStateStore.setFormState('test-id2', { test: 'test' })
        const form = UIStateStore.getForm('test-id2')
        expect(form).toEqual({ id: 'test-id2', test: 'test' })
      })
    })
    describe('getFormIndex tests', () => {
      it('if there are no forms, it returns -1', () => {
        UIStateStore.formsState.clear()
        expect(UIStateStore.formsState).toHaveLength(0)
        const form = UIStateStore.getFormIndex('test-id')
        expect(form).toBe(-1)
      })
      it('if there are no forms with the id, it returns -1', () => {
        UIStateStore.formsState.clear()
        expect(UIStateStore.formsState).toHaveLength(0)
        UIStateStore.setFormState('test-id', { test: 'test' })
        const form = UIStateStore.getFormIndex('test-id2')
        expect(form).toBe(-1)
      })
      it('if there is a form with the id, it returns the index', () => {
        UIStateStore.formsState.clear()
        expect(UIStateStore.formsState).toHaveLength(0)
        UIStateStore.setFormState('test-id', { test: 'test' })
        UIStateStore.setFormState('test-id2', { test: 'test' })
        const form = UIStateStore.getFormIndex('test-id2')
        expect(form).toBe(1)
      })
    })

    describe('setFormsState tests', () => {
      it('if there are no forms, it will add a form', () => {
        UIStateStore.formsState.clear()
        expect(UIStateStore.formsState).toHaveLength(0)
        UIStateStore.setFormState('test-id', { test: 'test' })
        expect(UIStateStore.formsState).toHaveLength(1)
        expect(UIStateStore.formsState[0]).toEqual({ id: 'test-id', test: 'test' })
      })
      it('if there are no form with the id, it will add a form', () => {
        UIStateStore.formsState.clear()
        expect(UIStateStore.formsState).toHaveLength(0)

        UIStateStore.setFormState('test-id', { test: 'test' })
        expect(UIStateStore.formsState[0]).toEqual({ id: 'test-id', test: 'test' })
        expect(UIStateStore.formsState).toHaveLength(1)

        UIStateStore.setFormState('test-id2', { test: 'test' })
        expect(UIStateStore.formsState[1]).toEqual({ id: 'test-id2', test: 'test' })
        expect(UIStateStore.formsState).toHaveLength(2)
      })
      it('if there is a form with the same id, it will overwrite', () => {
        UIStateStore.formsState.clear()
        expect(UIStateStore.formsState).toHaveLength(0)

        UIStateStore.setFormState('test-id', { test: 'test' })
        expect(UIStateStore.formsState).toHaveLength(1)
        expect(UIStateStore.formsState[0]).toEqual({ id: 'test-id', test: 'test' })

        UIStateStore.setFormState('test-id', { test: 'test2', test2: 'test3' })
        expect(UIStateStore.formsState).toHaveLength(1)
        expect(UIStateStore.formsState[0]).toEqual({ id: 'test-id', test: 'test2', test2: 'test3' })
      })
    })
    describe('changeFormsState tests', () => {
      it('if there are no forms, it will add a form', () => {
        UIStateStore.formsState.clear()
        expect(UIStateStore.formsState).toHaveLength(0)

        UIStateStore.changeFormState('test-id', 'test', 'test')
        expect(UIStateStore.formsState).toHaveLength(1)
        expect(UIStateStore.formsState[0]).toEqual({ id: 'test-id', test: 'test' })
      })
      it('if there are no form with the id, it will add a form', () => {
        UIStateStore.formsState.clear()
        expect(UIStateStore.formsState).toHaveLength(0)

        UIStateStore.changeFormState('test-id', 'test', 'test')
        expect(UIStateStore.formsState[0]).toEqual({ id: 'test-id', test: 'test' })
        expect(UIStateStore.formsState).toHaveLength(1)

        UIStateStore.changeFormState('test-id2', 'test', 'test')
        expect(UIStateStore.formsState[1]).toEqual({ id: 'test-id2', test: 'test' })
        expect(UIStateStore.formsState).toHaveLength(2)
      })
      it('if there is a form with the same id and a new key, it add to the form', () => {
        UIStateStore.formsState.clear()
        expect(UIStateStore.formsState).toHaveLength(0)
        UIStateStore.changeFormState('test-id', 'test', 'test')
        expect(UIStateStore.formsState).toHaveLength(1)
        expect(UIStateStore.formsState[0]).toEqual({ id: 'test-id', test: 'test' })

        UIStateStore.changeFormState('test-id', 'test2', 'test3')
        expect(UIStateStore.formsState).toHaveLength(1)
        expect(UIStateStore.formsState[0]).toEqual({ id: 'test-id', test: 'test', test2: 'test3' })
      })
      it('if there is a form with the same id and a same key, it will change the form', () => {
        UIStateStore.formsState.clear()
        expect(UIStateStore.formsState).toHaveLength(0)

        UIStateStore.setFormState('test-id', { test: 'test', test2: 'test3' })
        expect(UIStateStore.formsState).toHaveLength(1)
        expect(UIStateStore.formsState[0]).toEqual({ id: 'test-id', test: 'test', test2: 'test3' })

        UIStateStore.changeFormState('test-id', 'test2', 'test4')
        expect(UIStateStore.formsState).toHaveLength(1)
        expect(UIStateStore.formsState[0]).toEqual({ id: 'test-id', test: 'test', test2: 'test4' })
      })
    })
    describe('removeFormsState tests', () => {
      it('if there are no forms, it will do nothing', () => {
        UIStateStore.formsState.clear()
        expect(UIStateStore.formsState).toHaveLength(0)

        UIStateStore.removeForm('test-id')
        expect(UIStateStore.formsState).toHaveLength(0)
      })
      it('if there are no form with the id, it will do nothing', () => {
        UIStateStore.formsState.clear()
        expect(UIStateStore.formsState).toHaveLength(0)

        UIStateStore.setFormState('test-id', { test: 'test' })
        expect(UIStateStore.formsState[0]).toEqual({ id: 'test-id', test: 'test' })
        expect(UIStateStore.formsState).toHaveLength(1)

        UIStateStore.removeForm('test-id2')
        expect(UIStateStore.formsState[0]).toEqual({ id: 'test-id', test: 'test' })
        expect(UIStateStore.formsState).toHaveLength(1)
      })
      it('if there is a form with the same id, it will remove that form', () => {
        UIStateStore.formsState.clear()
        expect(UIStateStore.formsState).toHaveLength(0)

        UIStateStore.setFormState('test-id', { test: 'test' })
        UIStateStore.setFormState('test-id2', { test: 'test' })
        expect(UIStateStore.formsState[0]).toEqual({ id: 'test-id', test: 'test' })
        expect(UIStateStore.formsState).toHaveLength(2)

        UIStateStore.removeForm('test-id')
        expect(UIStateStore.formsState[0]).toEqual({ id: 'test-id2', test: 'test' })
        expect(UIStateStore.formsState).toHaveLength(1)
      })
    })
  })
})
