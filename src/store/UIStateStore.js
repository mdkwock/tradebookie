import { observable, action } from 'mobx'

class UIState {
  @observable formsState = []
  constructor() {
    this.changeFormState = this.changeFormState.bind(this)
    this.removeForm = this.removeForm.bind(this)
    this.getForm = this.getForm.bind(this)
    this.getFormIndex = this.getFormIndex.bind(this)
    this.setFormState = this.setFormState.bind(this)
  }
  getForm(formId) {
    const form = this.formsState.find((f) => { return f.get('id') === formId })
    if (form) {
      return form.toJS()
    }
    return form
  }
  getFormIndex(formId) {
    const formIndex = this.formsState.findIndex((form) => { return form.get('id') === formId })
    return formIndex
  }
  @action setFormState(formId, newFormState) {
    let formIndex = this.getFormIndex(formId)
    let form = null
    if (formIndex === -1) {
      const l = this.formsState.push(form)
      formIndex = l - 1
      form = observable.map({
        id: formId,
      })
    } else {
      form = this.formsState[formIndex]
    }
    form.merge(newFormState)
    this.formsState[formIndex] = form
  }
  @action changeFormState(formId, key, value) {
    let formIndex = this.getFormIndex(formId)
    let form = null
    if (formIndex === -1) {
      const l = this.formsState.push(form)
      formIndex = l - 1
      form = observable.map({
        id: formId,
      })
    } else {
      form = this.formsState[formIndex]
    }
    form.set(key, value)
    this.formsState[formIndex] = form
  }
  @action removeForm(formId) {
    const formIndex = this.getFormIndex(formId)
    if (formIndex !== -1) {
      this.formsState.remove(this.formsState[formIndex])
    }// if we can't find a form with that id, then we are good
  }
}

const singleton = new UIState()
export default singleton
