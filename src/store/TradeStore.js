import { action, observable } from 'mobx'
import Trade from './Trade'

export default class TradeStore {
  transportLayer
  @observable trades = []
  @observable isLoading = true

  constructor(transportLayer = null) {
    this.transportLayer = transportLayer // Thing that can make server requests for us
    this.getTrade = this.getTrade.bind(this)
    this.createTrade = this.createTrade.bind(this)
    this.loadTrades = this.loadTrades.bind(this)
    this.updateTrades = this.updateTrades.bind(this)
    this.updateTradeFromServer = this.updateTradeFromServer.bind(this)
    this.loadTrades()
  }

  /**
   * Fetches all trade's from the server
   */
  @action loadTrades() {
    this.isLoading = true
    this.transportLayer.getTrades().then(this.updateTrades)
  }

  updateTrades(trades) {
    trades.map(this.formatTradeFromServer).forEach(this.updateTradeFromServer)
    this.isLoading = false
  }

  formatTradeFromServer(json) {
    const newJson = json
    newJson.stockSymbol = json.stock_symbol
    return newJson
  }

  /**
   * Update a trade with information from the server. Guarantees a trade
   * only exists once. Might either construct a new trade, update an existing one,
   * or remove an trade if it has been deleted on the server.
   */
  @action updateTradeFromServer(json) {
    let trade = this.trades.find((t) => { return t.id === json.id })
    if (!trade) {
      trade = new Trade(this, json.id)
      this.trades.push(trade)
    }
    if (json.isDeleted) {
      this.removetrade(trade)
    } else {
      trade.updateTrade(json)
    }
  }

  getTrade(tradeId) {
    const trade = this.trades.find((t) => { return t.id === tradeId })
    return trade
  }

  /**
   * Creates a fresh trade on the client and server
   */
  @action createTrade(data) {
    const trade = new Trade(this)
    trade.updateTrade(data)
    this.trades.push(trade)
    this.transportLayer.submitTrade(trade.asJson)
    return trade
  }

  /**
   * A trade was somehow deleted, clean it from the client memory
   */
  @action removeTrade(tradeId) {
    const index = this.trades.findIndex((t) => { return t.id === tradeId })
    if (index !== -1) {
      this.trades.splice(index, 1)
    }
  }
}
