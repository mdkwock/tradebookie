import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import ApiService from './services/ApiService'
import UIStateStore from './store/UIStateStore'
import TradeStore from './store/TradeStore'
import './index.css'
import '../styles/base.scss'

const apiService = new ApiService('http://192.168.56.101/tradebookie')
const tradeStore = new TradeStore(apiService)

ReactDOM.render(
  <App UIState={UIStateStore} TradeStore={tradeStore} />,
  document.getElementById('root')
)
