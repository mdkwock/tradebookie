import React from 'react'
import ReactDOM from 'react-dom'
import AddTradePage from './AddTradePage'
import UIStateStore from './store/UIStateStore'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<AddTradePage UIState={UIStateStore} />, div)
})
