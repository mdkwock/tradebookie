import React from 'react'
import { observer } from 'mobx-react'
import Page from './Page'
import Portfolio from './components/Portfolio/Portfolio'
import './App.css'

const propTypes = {
  UIState: React.PropTypes.shape({
  }).isRequired,
  TradeStore: React.PropTypes.shape({
  }).isRequired,
}

const PortfolioPage = observer(({ UIState, TradeStore }) => {
  return (
    <Page title="Portfolio">
      <Portfolio UIState={UIState} TradeStore={TradeStore} />
    </Page>
  )
})

PortfolioPage.propTypes = propTypes

export default PortfolioPage
