const { fetch } = require('fetch-ponyfill')()


class ApiService {
  baseUrl = ''

  constructor(baseUrl) {
    this.baseUrl = baseUrl
  }

  submitTrade({
    timestamp,
    stockSymbol,
    price,
    quantity,
    type,
    tags
  }) {
    const formDataObj = {
      timestamp,
      stockSymbol,
      price,
      quantity,
      type,
      tags,
    }
    const formData = new FormData()
    Object.keys(formDataObj).forEach(key => { formData.append(key, formDataObj[key]) })
    fetch(`${this.baseUrl}/trade/add`, {
      method: 'POST',
      body: formData,
    }).then((response) => {
      return response.text()
    }).then((body) => {
      return body
    })
  }

  getTrades() {
    const tradePromise = fetch(`${this.baseUrl}/trades`).then((response) => {
      return response.json()
    }).then((body) => {
      return body
    })
    return tradePromise
  }
}

export default ApiService
