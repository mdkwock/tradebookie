import React from 'react'
import renderer from 'react-test-renderer'
import Link from '../Link'

describe('Link Tests', () => {
  it('Link renders properly', () => {
    const component = renderer.create(
      <Link
        text="test"
        href="http://google.com"
        onClick={() => {}}
      />
    )
    const tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })
  it('Link onclick works', () => {

  })
})
