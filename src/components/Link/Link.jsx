import React from 'react'
import { Link as RouterLink } from 'react-router-dom'

const propTypes = {
}

const defaultProps = {
}

const Link = props => {
  return (
    <RouterLink {...props} />
  )
}

Link.propTypes = propTypes
Link.defaultProps = defaultProps

export default Link
