import React from 'react'
import Logo from '../Logo/Logo'
import Nav from '../Nav/Nav'
import routes from '../../routes'

const Header = () => {
  return (
    <header className="Header m-header">
      <div className="top-bar">
        <Logo />
        <Nav
          links={routes.map((route) => {
            return {
              text: route.text,
              to: route.path,
            }
          })}
        />
      </div>
    </header>
  )
}

export default Header
