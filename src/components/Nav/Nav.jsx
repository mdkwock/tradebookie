import React from 'react'
import Link from '../Link/Link'
import './Nav.css'

const propTypes = {
  links: React.PropTypes.arrayOf(React.PropTypes.shape({
    text: React.PropTypes.string,
    to: React.PropTypes.string,
    onClick: React.PropTypes.func,
  })).isRequired,
}

const Nav = ({ links }) => {
  return (
    <nav className="Nav">{links.map((l) => {
      return (
        <Link key={l.to + l.text} to={l.to} onClick={l.onClick}>{l.text}</Link>
      )
    })}</nav>
  )
}

Nav.propTypes = propTypes

export default Nav
