import React from 'react'
import renderer from 'react-test-renderer'
import Nav from '../Nav'

test('Nav renders properly', () => {
  const component = renderer.create(
    <Nav
      links={[
        {
          text: 'text',
          href: 'http://google.com',
          onClick: () => {},
        },
      ]}
    />
  )
  const tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})
