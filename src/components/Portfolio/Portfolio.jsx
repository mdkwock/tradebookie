import React from 'react'
import { observer } from 'mobx-react'
import Table from '../Table/Table'

const propTypes = {
  TradeStore: React.PropTypes.shape({
    loadtTrades: React.PropTypes.func,
  }).isRequired
}

const Portfolio = observer(({ TradeStore }) => {
  return (
    <Table
      isLoading={TradeStore.isLoading}
      data={TradeStore.trades.peek().map((trade) => {
        return [
          {
            content: trade.timestamp
          },
          {
            content: trade.stockSymbol
          },
          {
            content: trade.quantity
          },
          {
            content: trade.price
          },
          {
            content: trade.price
          },
          {
            content: trade.price - trade.price
          },
        ]
      })}
      headers={[
        {
          content: 'Purchase Date'
        },
        {
          content: 'Stock Symbol'
        },
        {
          content: 'Qty'
        },
        {
          content: 'Cost'
        },
        {
          content: 'Current'
        },
        {
          content: 'Profit'
        },
      ]}
    />
  )
})

Portfolio.propTypes = propTypes

export default Portfolio
