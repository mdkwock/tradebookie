import React from 'react'
import logo from './logo.svg'

const Logo = () => {
  return (
    <div className="Logo"><img src={logo} className="App-logo" alt="logo" /></div>
  )
}

export default Logo
