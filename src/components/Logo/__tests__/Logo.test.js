import React from 'react'
import renderer from 'react-test-renderer'
import Logo from '../Logo'

test('Logo renders properly', () => {
  const component = renderer.create(
    <Logo src="logo.png" />
  )
  const tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})
