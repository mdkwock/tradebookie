import React from 'react'

const Details = () => {
  return (
    <details className="m-details">
      <summary className="m-details-title">Instructions</summary>
      <div className="m-details-body">
        <p>
          {"This is the world's EASIEST trade entry Form. Just enter in the Ticker, and we'll look up the price for you."}
        </p>
      </div>
    </details>
  )
}

export default Details
