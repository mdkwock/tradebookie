import React, { Component } from 'react'
import TableCell from './TableCell'
import TableHeader from './TableHeader'
import TableRow from './TableRow'

const propTypes = {
  className: React.PropTypes.string,
  data: React.PropTypes.arrayOf(
    React.PropTypes.arrayOf(React.PropTypes.shape({
      content: React.PropTypes.node,
    }))
  ).isRequired,
  headers: React.PropTypes.arrayOf(
    React.PropTypes.shape({
      content: React.PropTypes.node,
    })
  ).isRequired,
  onCellClick: React.PropTypes.func,
  onHeaderClick: React.PropTypes.func,
  onMouseOver: React.PropTypes.func,
}

const defaultProps = {
  className: '',
  onCellClick: null,
  onHeaderClick: null,
  onMouseOver: null,
}

class Table extends Component {
  constructor(props) {
    super(props)
    this.renderRows = this.renderRows.bind(this)
    this.renderRow = this.renderRow.bind(this)
    this.renderHeader = this.renderHeader.bind(this)
    this.renderHeaderCell = this.renderHeaderCell.bind(this)
    this.handleHeaderClick = this.handleHeaderClick.bind(this)
    this.handleMouseOver = this.handleMouseOver.bind(this)
  }
  handleMouseOver(event, tableRow) {
    if (this.props.onMouseOver) {
      this.props.onMouseOver(event, tableRow.props.index)
    }
  }
  handleHeaderClick(event, tableHeader) {
    if (this.props.onHeaderClick) {
      this.props.onHeaderClick(event, tableHeader.props.index)
    }
  }
  handleCellClick(event, tableCell) {
    if (this.props.onCellClick) {
      this.props.onCellClick(event, tableCell.props.index)
    }
  }

  renderHeader(headers) {
    return headers.map(this.renderHeaderCell)
  }
  renderRows(rows) {
    return rows.map(this.renderRow)
  }
  renderRow(dataRow, rowIndex) {
    return (
      <TableRow key={rowIndex} index={rowIndex} onMouseOver={this.handleMouseOver}>
        {dataRow.map((cell, cellIndex) => {
          return this.renderCell(cell, rowIndex, cellIndex)
        })}
      </TableRow>
    )
  }

  renderHeaderCell(headerCell, index) {
    return (
      <TableHeader {...headerCell} index={index} key={index} onClick={this.handleHeaderClick}>{headerCell.content}</TableHeader>
    )
  }

  renderCell(cell, rowIndex, cellIndex) {
    return (
      <TableCell {...cell} key={`${rowIndex}-${cellIndex}`}>{cell.content}</TableCell>
    )
  }
  render() {
    return (
      <table className={`table ${this.props.className}`}>
        <thead>
          <TableRow className="table-row-header">
            {this.renderHeader(this.props.headers)}
          </TableRow>
        </thead>
        <tbody>
          {this.renderRows(this.props.data)}
        </tbody>
      </table>
    )
  }
}

Table.propTypes = propTypes
Table.defaultProps = defaultProps

export default Table
