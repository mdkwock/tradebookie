import React, { Component } from 'react'

const propTypes = {
  index: React.PropTypes.number,
  onClick: React.PropTypes.func,
  onMouseOver: React.PropTypes.func,
  className: React.PropTypes.string,
  children: React.PropTypes.node.isRequired,
}

const defaultProps = {
  index: 0,
  className: '',
  onClick: null,
  onMouseOver: null,
}

class TableCell extends Component {
  constructor(props) {
    super(props)
    this.handleMouseOver = this.handleMouseOver.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }
  handleMouseOver(event) {
    if (this.props.onMouseOver) {
      this.props.onMouseOver(event, this, this.props.index)
    }
  }
  handleClick(event) {
    if (this.props.onClick) {
      this.props.onClick(event, this, this.props.index)
    }
  }
  render() {
    return (
      <td {...this.props} className={this.props.className} onMouseOver={this.handleMouseOver} onClick={this.handleClick}>
        {this.props.children}
      </td>
    )
  }
}

TableCell.propTypes = propTypes
TableCell.defaultProps = defaultProps

export default TableCell
