import React, { Component } from 'react'

const propTypes = {
  children: React.PropTypes.node.isRequired,
  className: React.PropTypes.string,
  index: React.PropTypes.number,
  onClick: React.PropTypes.func,
  onMouseOver: React.PropTypes.func,
}

const defaultProps = {
  className: '',
  index: 0,
  onClick: null,
  onMouseOver: null,
}

class TableRow extends Component {
  constructor(props) {
    super(props)
    this.handleMouseOver = this.handleMouseOver.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }
  handleMouseOver(event) {
    if (this.props.onMouseOver) {
      this.props.onMouseOver(event, this, this.props.index)
    }
  }
  handleClick(event) {
    if (this.props.onClick) {
      this.props.onClick(event, this, this.props.index)
    }
  }
  render() {
    return (
      <TR className={this.props.className} onMouseOver={this.handleMouseOver} onClick={this.handleClick} >
        {this.props.children}
      </TR>
    )
  }
}

const TRPropTypes = {
  children: React.PropTypes.node.isRequired,
  className: React.PropTypes.string.isRequired,
  onClick: React.PropTypes.func.isRequired,
  onMouseOver: React.PropTypes.func.isRequired,
}

const TR = ({ children, className, onMouseOver, onClick }) => {
  return (
    <tr className={className} onClick={onClick} onMouseOver={onMouseOver}>{children}</tr>
  )
}

TR.propTypes = TRPropTypes

TableRow.propTypes = propTypes
TableRow.defaultProps = defaultProps

export default TableRow
