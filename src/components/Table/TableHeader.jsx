import React, { Component } from 'react'
import classnames from 'classnames'

const propTypes = {
  className: React.PropTypes.string,
  index: React.PropTypes.number,
  onClick: React.PropTypes.func,
  onMouseOver: React.PropTypes.func,
  sortable: React.PropTypes.bool,
  children: React.PropTypes.node.isRequired,
}

const defaultProps = {
  className: '',
  index: 0,
  onClick: null,
  onMouseOver: null,
  sortable: false,
}

class TableHeader extends Component {
  constructor(props) {
    super(props)
    this.handleMouseOver = this.handleMouseOver.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }
  handleMouseOver(event) {
    if (this.props.onMouseOver) {
      this.props.onMouseOver(event, this, this.props.index)
    }
  }
  handleClick(event) {
    if (this.props.onClick) {
      this.props.onClick(event, this, this.props.index)
    }
  }

  render() {
    const className = classnames(this.props.className, { sortable: this.props.sortable })
    return (
      <th {...this.props} className={className} scope="col" onMouseOver={this.handleMouseOver} onClick={this.handleClick}>
        {this.props.children}
      </th>
    )
  }
}

TableHeader.propTypes = propTypes
TableHeader.defaultProps = defaultProps

export default TableHeader
