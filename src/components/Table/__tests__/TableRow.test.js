import React from 'react'
import { shallow, render } from 'enzyme'
import TableRow from '../TableRow'

// we can import styles as css modules and compare created class names with original ones
// import style from '../app/components/common/styles';

// Shallow Rendering
// https://github.com/airbnb/enzyme/blob/master/docs/api/shallow.md
describe('TableRow HTML', () => {
  it('renders correctly', () => {
    const component = render(
      <TableRow
        index={5}
      >{'this is a child text'}</TableRow>
    )
    expect(component).toMatchSnapshot()
  })
  describe('interaction tests', () => {
    it('mouse over works', () => {
      const mouseOverSpy = jest.fn()
      const event = { test: 'test' }
      const tableRow = shallow(<TableRow
        index={5}
        onMouseOver={mouseOverSpy}
      >
        {'this is a cell'}
      </TableRow>)
      tableRow.simulate('mouseOver', event)
      expect(mouseOverSpy).toHaveBeenCalledTimes(1)
      const args = mouseOverSpy.mock.calls[0]
      expect(args[0]).toBe(event)
      expect(args[1]).toBeInstanceOf(TableRow)
    })
    it('onclick works', () => {
      const clickSpy = jest.fn()
      const event = { test: 'test' }
      const tableRow = shallow(<TableRow
        index={5}
        onClick={clickSpy}
      >
        {'this is a cell'}
      </TableRow>)
      tableRow.simulate('click', event)
      expect(clickSpy).toHaveBeenCalledTimes(1)
      const args = clickSpy.mock.calls[0]
      expect(args[0]).toBe(event)
      expect(args[1]).toBeInstanceOf(TableRow)
    })
  })
})
