import React from 'react'
import { shallow } from 'enzyme'
import Table from '../Table'

// we can import styles as css modules and compare created class names with original ones
// import style from '../app/components/common/styles';

// Shallow Rendering
// https://github.com/airbnb/enzyme/blob/master/docs/api/shallow.md
describe('Table HTML', () => {
  it('renders correctly', () => {
    const headers = [
      {
        content: 'test',
        'aria-labelledby': 'this is a test',
        'data-hi': 'test',
      },
      {
        content: 'two',
      },
    ]
    const data = Array(2).fill(headers)
    const table = shallow(<Table
      headers={headers}
      data={data}
    />)
    expect(table).toMatchSnapshot()
  })
})
