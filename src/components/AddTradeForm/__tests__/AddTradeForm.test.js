import React from 'react'
import { shallow } from 'enzyme'
import AddTradeForm from '../AddTradeForm'

describe('AddTradeForm Tests', () => {
  const formState = {
    stockSymbol: 'test',
    timestamp: '2014/2/2',
    actionType: 'buy',
    price: 0,
  }
  test('AddTradeForm renders properly', () => {
    const tempFormState = formState
    const component = shallow(
      <AddTradeForm
        setFormState={() => {}}
        changeFormState={() => {}}
        formState={tempFormState}
        id=""
      />
    )
    expect(component).toMatchSnapshot()
  })
  it('change the stock symbol', () => {
    // change the stock symbol
    const tempFormState = formState
    let component = shallow(
      <AddTradeForm
        setFormState={() => {}}
        changeFormState={() => {}}
        formState={tempFormState}
        id=""
      />
    )
    // re-rendering
    expect(component).toMatchSnapshot()

    tempFormState.stockSymbol = 'test2'
    component = shallow(
      <AddTradeForm
        setFormState={() => {}}
        changeFormState={() => {}}
        formState={tempFormState}
        id=""
      />
    )
    // re-rendering
    expect(component).toMatchSnapshot()
  })
  describe('timestamp tests', () => {
    it('change the timestamp', () => {
      // change the stock symbol
      const tempFormState = formState
      let component = shallow(
        <AddTradeForm
          setFormState={() => {}}
          changeFormState={() => {}}
          formState={tempFormState}
          id=""
        />
      )
      // re-rendering
      expect(component).toMatchSnapshot()

      tempFormState.stockSymbol = '2014/2/3'
      component = shallow(
        <AddTradeForm
          setFormState={() => {}}
          changeFormState={() => {}}
          formState={tempFormState}
          id=""
        />
      )
      // re-rendering
      expect(component).toMatchSnapshot()
    })
    it.skip('invalid timestamp, shows an error', () => {
      // change the stock symbol
      const tempFormState = formState
      let component = shallow(
        <AddTradeForm
          setFormState={() => {}}
          changeFormState={() => {}}
          formState={tempFormState}
          id=""
        />
      )
      // re-rendering
      expect(component).toMatchSnapshot()

      tempFormState.timestamp = '2014/2/3'
      component = shallow(
        <AddTradeForm
          setFormState={() => {}}
          changeFormState={() => {}}
          formState={tempFormState}
          id=""
        />
      )
      // re-rendering
      expect(component).toMatchSnapshot()
    })
  })
  it('change the action type', () => {
    // change the stock symbol
    const tempFormState = formState
    let component = shallow(
      <AddTradeForm
        setFormState={() => {}}
        changeFormState={() => {}}
        formState={tempFormState}
        id=""
      />
    )
    // re-rendering
    expect(component).toMatchSnapshot()

    tempFormState.actionType = 'sell'
    component = shallow(
      <AddTradeForm
        setFormState={() => {}}
        changeFormState={() => {}}
        formState={tempFormState}
        id=""
      />
    )
    // re-rendering
    expect(component).toMatchSnapshot()
  })
  describe('price tests', () => {
    it('change the price', () => {
      // change the stock symbol
      const tempFormState = formState
      let component = shallow(
        <AddTradeForm
          setFormState={() => {}}
          changeFormState={() => {}}
          formState={tempFormState}
          id=""
        />
      )
      // re-rendering
      expect(component).toMatchSnapshot()

      tempFormState.price = 9
      component = shallow(
        <AddTradeForm
          setFormState={() => {}}
          changeFormState={() => {}}
          formState={tempFormState}
          id=""
        />
      )
      // re-rendering
      expect(component).toMatchSnapshot()
    })
    it.skip('not numbers throw an error', () => {
    })
  })
})
