import React, { Component } from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import { observer } from 'mobx-react'
import DateTime from 'react-datetime'
import './style/style.scss'
import '../Button/style/Button.scss'

const propTypes = {
  formState: React.PropTypes.shape({
    stockSymbol: React.PropTypes.string,
    timestamp: React.PropTypes.date,
    actionType: React.PropTypes.string,
    price: React.PropTypes.number,
  }),
  setFormState: React.PropTypes.func.isRequired,
  changeFormState: React.PropTypes.func.isRequired,
  onSubmit: React.PropTypes.func.isRequired,
  id: React.PropTypes.string,
}

const defaultProps = {
  formState: {
    stockSymbol: '',
    timestamp: new Date(),
    actionType: 'buy',
    price: 0,
  },
  id: 'add-trade-form',
}

@observer class AddTradeForm extends Component {
  constructor(props) {
    super(props)
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleDateChange = this.handleDateChange.bind(this)
    this.componentWillMount = this.componentWillMount.bind(this)
  }
  componentWillMount() {
    this.props.setFormState(this.props.id, this.props.formState)
  }
  handleChange(event) {
    const target = event.target
    this.props.changeFormState(this.props.id, target.name, target.value)
  }
  handleDateChange(moment) {
    this.handleChange({ target: {
      name: 'timestamp',
      value: moment,
    } })
    // this.props.changeFormState(this.props.id, 'timestamp', moment)
  }
  handleSubmit(event) {
    event.preventDefault()
    this.props.onSubmit(this.props.formState)
  }
  render() {
    return (
      <form id={this.props.id} onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label htmlFor={`${this.props.id}_stockSymbol`}>Stock Ticker</label>
          <input onChange={this.handleChange} type="text" name="stockSymbol" value={this.props.formState.stockSymbol} id={`${this.props.id}_stockSymbol`} />
        </div>
        <div className="form-group">
          <label htmlFor={`${this.props.id}_timestamp`}>Time and Date</label>
          <DateTime
            onChange={this.handleDateChange}
            onUpdate={this.handleChange}
            value={this.props.formState.timestamp}
            closeOnSelect
          />
        </div>
        <div className="form-group">
          <label className="m-input-radio" htmlFor={`${this.props.id}_buy`}>
            Buy<input onChange={this.handleChange} type="radio" name="actionType" value="buy" id={`${this.props.id}_buy`} checked={this.props.formState.actionType === 'buy'} />
          </label>
          <label className="m-input-radio" htmlFor={`${this.props.id}_sell`}>
            Sell<input onChange={this.handleChange} type="radio" name="actionType" value="sell" id={`${this.props.id}_sell`} checked={this.props.formState.actionType === 'sell'} />
          </label>
        </div>
        <div className="form-group">
          <label htmlFor={`${this.props.id}_price`}>Price</label>
          <input onChange={this.handleChange} type="text" name="price" value={this.props.formState.price} id={`${this.props.id}_price`} />
        </div>
        <div className="form-group form-group--button">
          <button type="submit" className="m-button success">Save Trade</button>
        </div>
      </form>
    )
  }
}

AddTradeForm.propTypes = propTypes
AddTradeForm.defaultProps = defaultProps

export default AddTradeForm
