import React from 'react'
import Header from './components/Header/Header'
import Footer from './components/Footer/Footer'

const propTypes = {
  title: React.PropTypes.string.isRequired,
  children: React.PropTypes.node.isRequired,
}

const Page = ({ title, children }) => {
  return (
    <div className="Page">
      <Header />
      <main>
        <h2>{title}</h2>
        {children}
      </main>
      <Footer />
    </div>
  )
}

Page.propTypes = propTypes
export default Page
