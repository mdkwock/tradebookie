import AddTradePage from './AddTradePage'
import PortfolioPage from './PortfolioPage'

const routes = [
  {
    text: 'Add a Trade',
    path: '/add-trade',
    component: AddTradePage
  },
  {
    text: 'Portfolio',
    path: '/portfolio',
    component: PortfolioPage
  },
]

export default routes
